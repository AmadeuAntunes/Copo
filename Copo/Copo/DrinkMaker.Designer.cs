﻿namespace Copo
{
    partial class DrinkMaker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.icetea = new System.Windows.Forms.Button();
            this.cocacola = new System.Windows.Forms.Button();
            this.sumol = new System.Windows.Forms.Button();
            this.pepsi = new System.Windows.Forms.Button();
            this.groupBoxBebidas = new System.Windows.Forms.GroupBox();
            this.Copos = new System.Windows.Forms.GroupBox();
            this.btn200ml = new System.Windows.Forms.Button();
            this.btn150ml = new System.Windows.Forms.Button();
            this.btn110ml = new System.Windows.Forms.Button();
            this.btn50ml = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.btnEncher = new System.Windows.Forms.Button();
            this.btnesvaziar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Qliquido = new System.Windows.Forms.NumericUpDown();
            this.Informação = new System.Windows.Forms.GroupBox();
            this.info = new System.Windows.Forms.Label();
            this.lblPerc = new System.Windows.Forms.Label();
            this.pb = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Copos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Qliquido)).BeginInit();
            this.Informação.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // icetea
            // 
            this.icetea.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.icetea.Location = new System.Drawing.Point(30, 84);
            this.icetea.Name = "icetea";
            this.icetea.Size = new System.Drawing.Size(143, 73);
            this.icetea.TabIndex = 1;
            this.icetea.Text = "Ice Tea";
            this.icetea.UseVisualStyleBackColor = true;
            this.icetea.Click += new System.EventHandler(this.icetea_Click);
            // 
            // cocacola
            // 
            this.cocacola.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cocacola.Location = new System.Drawing.Point(176, 85);
            this.cocacola.Name = "cocacola";
            this.cocacola.Size = new System.Drawing.Size(162, 73);
            this.cocacola.TabIndex = 2;
            this.cocacola.Text = "Coca-Cola";
            this.cocacola.UseVisualStyleBackColor = true;
            this.cocacola.Click += new System.EventHandler(this.cocacola_Click);
            // 
            // sumol
            // 
            this.sumol.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sumol.Location = new System.Drawing.Point(342, 85);
            this.sumol.Name = "sumol";
            this.sumol.Size = new System.Drawing.Size(166, 73);
            this.sumol.TabIndex = 3;
            this.sumol.Text = "Sumol";
            this.sumol.UseVisualStyleBackColor = true;
            this.sumol.Click += new System.EventHandler(this.sumol_Click);
            // 
            // pepsi
            // 
            this.pepsi.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pepsi.Location = new System.Drawing.Point(512, 85);
            this.pepsi.Name = "pepsi";
            this.pepsi.Size = new System.Drawing.Size(143, 73);
            this.pepsi.TabIndex = 4;
            this.pepsi.Text = "Pepsi";
            this.pepsi.UseVisualStyleBackColor = true;
            this.pepsi.Click += new System.EventHandler(this.pepsi_Click);
            // 
            // groupBoxBebidas
            // 
            this.groupBoxBebidas.Location = new System.Drawing.Point(12, 66);
            this.groupBoxBebidas.Name = "groupBoxBebidas";
            this.groupBoxBebidas.Size = new System.Drawing.Size(660, 108);
            this.groupBoxBebidas.TabIndex = 5;
            this.groupBoxBebidas.TabStop = false;
            this.groupBoxBebidas.Text = "Bebidas";
            // 
            // Copos
            // 
            this.Copos.Controls.Add(this.btn200ml);
            this.Copos.Controls.Add(this.btn150ml);
            this.Copos.Controls.Add(this.btn110ml);
            this.Copos.Controls.Add(this.btn50ml);
            this.Copos.Location = new System.Drawing.Point(12, 180);
            this.Copos.Name = "Copos";
            this.Copos.Size = new System.Drawing.Size(660, 108);
            this.Copos.TabIndex = 6;
            this.Copos.TabStop = false;
            this.Copos.Text = "Tamanho do Copos";
            // 
            // btn200ml
            // 
            this.btn200ml.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn200ml.Location = new System.Drawing.Point(500, 18);
            this.btn200ml.Name = "btn200ml";
            this.btn200ml.Size = new System.Drawing.Size(143, 73);
            this.btn200ml.TabIndex = 8;
            this.btn200ml.Text = "200 ml";
            this.btn200ml.UseVisualStyleBackColor = true;
            this.btn200ml.Click += new System.EventHandler(this.btn200ml_Click);
            // 
            // btn150ml
            // 
            this.btn150ml.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn150ml.Location = new System.Drawing.Point(330, 18);
            this.btn150ml.Name = "btn150ml";
            this.btn150ml.Size = new System.Drawing.Size(166, 73);
            this.btn150ml.TabIndex = 7;
            this.btn150ml.Text = "150 ml";
            this.btn150ml.UseVisualStyleBackColor = true;
            this.btn150ml.Click += new System.EventHandler(this.btn150ml_Click);
            // 
            // btn110ml
            // 
            this.btn110ml.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn110ml.Location = new System.Drawing.Point(164, 18);
            this.btn110ml.Name = "btn110ml";
            this.btn110ml.Size = new System.Drawing.Size(162, 73);
            this.btn110ml.TabIndex = 6;
            this.btn110ml.Text = "110 ml";
            this.btn110ml.UseVisualStyleBackColor = true;
            this.btn110ml.Click += new System.EventHandler(this.btn110ml_Click);
            // 
            // btn50ml
            // 
            this.btn50ml.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn50ml.Location = new System.Drawing.Point(18, 17);
            this.btn50ml.Name = "btn50ml";
            this.btn50ml.Size = new System.Drawing.Size(143, 73);
            this.btn50ml.TabIndex = 5;
            this.btn50ml.Text = "50 ml";
            this.btn50ml.UseVisualStyleBackColor = true;
            this.btn50ml.Click += new System.EventHandler(this.btn50ml_Click);
            // 
            // progressBar
            // 
            this.progressBar.AccessibleRole = System.Windows.Forms.AccessibleRole.Alert;
            this.progressBar.Location = new System.Drawing.Point(12, 516);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(659, 23);
            this.progressBar.TabIndex = 7;
            // 
            // btnEncher
            // 
            this.btnEncher.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEncher.Location = new System.Drawing.Point(12, 375);
            this.btnEncher.Name = "btnEncher";
            this.btnEncher.Size = new System.Drawing.Size(161, 88);
            this.btnEncher.TabIndex = 8;
            this.btnEncher.Text = "Encher";
            this.btnEncher.UseVisualStyleBackColor = true;
            this.btnEncher.Click += new System.EventHandler(this.btnEncher_Click);
            // 
            // btnesvaziar
            // 
            this.btnesvaziar.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnesvaziar.Location = new System.Drawing.Point(512, 375);
            this.btnesvaziar.Name = "btnesvaziar";
            this.btnesvaziar.Size = new System.Drawing.Size(161, 88);
            this.btnesvaziar.TabIndex = 9;
            this.btnesvaziar.Text = "Esvaziar";
            this.btnesvaziar.UseVisualStyleBackColor = true;
            this.btnesvaziar.Click += new System.EventHandler(this.btnesvaziar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 308);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(229, 25);
            this.label1.TabIndex = 12;
            this.label1.Text = "Quantidade de Liquido";
            // 
            // Qliquido
            // 
            this.Qliquido.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Qliquido.Location = new System.Drawing.Point(254, 307);
            this.Qliquido.Maximum = new decimal(new int[] {
            -1,
            -1,
            -1,
            0});
            this.Qliquido.Name = "Qliquido";
            this.Qliquido.Size = new System.Drawing.Size(120, 31);
            this.Qliquido.TabIndex = 14;
            // 
            // Informação
            // 
            this.Informação.Controls.Add(this.info);
            this.Informação.Location = new System.Drawing.Point(13, 545);
            this.Informação.Name = "Informação";
            this.Informação.Size = new System.Drawing.Size(660, 54);
            this.Informação.TabIndex = 15;
            this.Informação.TabStop = false;
            this.Informação.Text = "Informações";
            // 
            // info
            // 
            this.info.AutoSize = true;
            this.info.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.info.Location = new System.Drawing.Point(17, 20);
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(60, 24);
            this.info.TabIndex = 0;
            this.info.Text = "label2";
            // 
            // lblPerc
            // 
            this.lblPerc.AutoSize = true;
            this.lblPerc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.lblPerc.Location = new System.Drawing.Point(316, 493);
            this.lblPerc.Name = "lblPerc";
            this.lblPerc.Size = new System.Drawing.Size(26, 20);
            this.lblPerc.TabIndex = 16;
            this.lblPerc.Text = "- -";
            // 
            // pb
            // 
            this.pb.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pb.Image = global::Copo.Properties.Resources.copovazio;
            this.pb.InitialImage = null;
            this.pb.Location = new System.Drawing.Point(242, 357);
            this.pb.Margin = new System.Windows.Forms.Padding(0);
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(201, 134);
            this.pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb.TabIndex = 10;
            this.pb.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(143)))), ((int)(((byte)(221)))));
            this.pictureBox1.Location = new System.Drawing.Point(-3, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(691, 60);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // DrinkMaker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(234)))), ((int)(((byte)(234)))));
            this.ClientSize = new System.Drawing.Size(684, 608);
            this.Controls.Add(this.lblPerc);
            this.Controls.Add(this.Informação);
            this.Controls.Add(this.Qliquido);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pb);
            this.Controls.Add(this.btnesvaziar);
            this.Controls.Add(this.btnEncher);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.Copos);
            this.Controls.Add(this.pepsi);
            this.Controls.Add(this.sumol);
            this.Controls.Add(this.cocacola);
            this.Controls.Add(this.icetea);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBoxBebidas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "DrinkMaker";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DrinkMaker";
            this.Copos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Qliquido)).EndInit();
            this.Informação.ResumeLayout(false);
            this.Informação.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button icetea;
        private System.Windows.Forms.Button cocacola;
        private System.Windows.Forms.Button sumol;
        private System.Windows.Forms.Button pepsi;
        private System.Windows.Forms.GroupBox groupBoxBebidas;
        private System.Windows.Forms.GroupBox Copos;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Button btnEncher;
        private System.Windows.Forms.Button btnesvaziar;
        private System.Windows.Forms.PictureBox pb;
        private System.Windows.Forms.Button btn200ml;
        private System.Windows.Forms.Button btn150ml;
        private System.Windows.Forms.Button btn110ml;
        private System.Windows.Forms.Button btn50ml;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown Qliquido;
        private System.Windows.Forms.GroupBox Informação;
        private System.Windows.Forms.Label info;
        private System.Windows.Forms.Label lblPerc;
    }
}