﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Copo
{
    

    public partial class loadForm : Form
    {
        Timer MyTimer = new Timer();
        public loadForm()
        {
          
            MyTimer.Interval = (2000); 
            MyTimer.Tick += new EventHandler(MyTimer_Tick);
            MyTimer.Start();
            InitializeComponent();
        }
        private void MyTimer_Tick(object sender, EventArgs e)
        {
            this.Hide();
            DrinkMaker form = new DrinkMaker();
            form.MdiParent = loadForm.ActiveForm;
            form.Show();
            MyTimer.Stop();
          

        }
    }
}
