﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Copo
{
    public partial class DrinkMaker : Form
    {
        Copo generico;
        Timer MyTimer = new Timer();
        private string bebida = String.Empty;
        private int capacidade = 0;
        bool somar = false;
        private int counter = 0;
        public DrinkMaker()
        {
            
            InitializeComponent();
            generico = new Copo();
            MyTimer.Tick += new EventHandler(MyTimer_Tick);
            messagem("Selecione uma bebida");
            pb.ImageLocation = Directory.GetCurrentDirectory() + @"\..\..\img\copovazio.png";
        }

        public void messagem(string msg)
        {
            info.Text = msg;
        }
        private void btnEncher_Click(object sender, EventArgs e)
        {
            if (bebida == String.Empty)
            {
                messagem("Selecione uma bebida");               
            } 
            else
            {
                if (capacidade == 0)
                {
                    messagem("Selecione o tamanho do copo");
                }
                else
                {
                    if (somar == false)
                    {
                        MyTimer.Stop();
                        somar = true;
                        MyTimer.Interval = (1000);
                        MyTimer.Start();
                        generico.Liquido = bebida;
                        generico.Capacidade = capacidade;
                        generico.Encher((double)Qliquido.Value);
                  
                    }
                }
            }
        }

        private void btnesvaziar_Click(object sender, EventArgs e)
        {
            if(somar == true)
            {
                MyTimer.Stop();
                somar = false;
                MyTimer.Interval = (1000);
                MyTimer.Start();
                generico.Esvaziar((double)Qliquido.Value);
               
            }
        }


        private void MyTimer_Tick(object sender, EventArgs e)
        {

            if (somar == true)
            {
                counter++;
            }
            else
            {
                counter--;
            }
            int per = generico.ValorEmPercentagem()/ 10;
            if(counter < generico.ValorEmPercentagem() )
            {
                messagem(generico.ToString());
                if (generico.ValorEmPercentagem() < 10)
                {
                    pb.ImageLocation = Directory.GetCurrentDirectory() + @"\..\..\img\copovazio.png";
                    lblPerc.Text = generico.ValorEmPercentagem().ToString() + "%";
                    progressBar.Value = generico.ValorEmPercentagem();
                    MyTimer.Stop();
                }
                else
                {
                    pb.ImageLocation = Directory.GetCurrentDirectory() + String.Concat(string.Concat(@"\..\..\img\copo", per.ToString()), ".png");
                    lblPerc.Text = generico.ValorEmPercentagem().ToString() + "%";
                    progressBar.Value = generico.ValorEmPercentagem();
                    MyTimer.Stop();
                }
                
            }

            else
            {
                
                messagem(generico.ToString());
                lblPerc.Text = generico.ValorEmPercentagem().ToString() + "%";
                progressBar.Value = generico.ValorEmPercentagem();
            }
        }

        private void icetea_Click(object sender, EventArgs e)
        {
            bebida = icetea.Text;
            messagem("Foi selecionada a bebida " + bebida);

        }

        private void cocacola_Click(object sender, EventArgs e)
        {
            bebida = cocacola.Text;
            messagem("Foi selecionada a bebida " + bebida);
        }

        private void sumol_Click(object sender, EventArgs e)
        {
            bebida = sumol.Text;
            messagem("Foi selecionada a bebida " + bebida);
        }

        private void pepsi_Click(object sender, EventArgs e)
        {
            bebida = pepsi.Text;
            messagem("Foi selecionada a bebida " + bebida);
        }

        private void btn50ml_Click(object sender, EventArgs e)
        {
            capacidade = 50;
            messagem("Foi selecionada a quantidade de " + capacidade.ToString());
        }  

        private void btn110ml_Click(object sender, EventArgs e)
        {
            capacidade = 110;
            messagem("Foi selecionada a quantidade de " + capacidade.ToString());
        }

        private void btn150ml_Click(object sender, EventArgs e)
        {
            capacidade = 150;
            messagem("Foi selecionada a quantidade de " + capacidade.ToString());
        }

        private void btn200ml_Click(object sender, EventArgs e)
        {
            capacidade = 200;
            messagem("Foi selecionada a quantidade de " + capacidade.ToString());
        }
    }
}
